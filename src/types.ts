export type AdviceSlip = {
  id: number;
  advice: string;
}

export type AdviceResponse = {
  slip: AdviceSlip;
}
